



INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');


INSERT INTO skakaonica (id, naziv, k, d) VALUES (1, 'Velika', 3.0, 2.8);
INSERT INTO skakaonica (id, naziv, k, d) VALUES (2, 'Mala', 1.0, 2.0);
INSERT INTO skakaonica (id, naziv, k, d) VALUES (3, 'Srednja', 1.2, 2.2);


INSERT INTO takmicar (id, ime_prezime, drzava, visina, e_mail, datum_rodjenja, skakaonica_id) VALUES (1, 'Jelena Kozic', 'Srbija', 160, 'jelena@gmail.com', '1991-12-18', 1);
INSERT INTO takmicar (id, ime_prezime, drzava, visina, e_mail, datum_rodjenja, skakaonica_id) VALUES (2, 'Marko Lagumdzija', 'Srbija', 195, 'marko@gmail.com', '1993-07-10', 2);
INSERT INTO takmicar (id, ime_prezime, drzava, visina, e_mail, datum_rodjenja, skakaonica_id) VALUES (3, 'Milana Tosic', 'Srbija', 160, 'milana@gmail.com', '1995-04-04', 3);
INSERT INTO takmicar (id, ime_prezime, drzava, visina, e_mail, datum_rodjenja, skakaonica_id) VALUES (4, 'Svetislav Marjanovic', 'Srbija', 190, 'sveta@gmail.com', '1993-07-23', 1);
INSERT INTO takmicar (id, ime_prezime, drzava, visina, e_mail, datum_rodjenja, skakaonica_id) VALUES (5, 'Srdjan Dobrijevic', 'Srbija', 185, 'srki@gmail.com', '1993-07-29', 2);


INSERT INTO skok (id, daljina, poeni_za_daljinu, ocena_sudija, zbir_poena, takmicar_id) VALUES (1, 30.0, 20.0, 5.0, 40.0, 1);
INSERT INTO skok (id, daljina, poeni_za_daljinu, ocena_sudija, zbir_poena, takmicar_id) VALUES (2, 20.0, 10.0, 4.0, 30.0, 2);
INSERT INTO skok (id, daljina, poeni_za_daljinu, ocena_sudija, zbir_poena, takmicar_id) VALUES (3, 10.0, 5.0, 3.0, 20.0, 2);