package zima.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Skakaonica;
import zima.service.SkakaonicaService;
import zima.web.dto.SkakaonicaDTO;


@Component
public class SkakaonicaDTOToSkakaonica implements Converter<SkakaonicaDTO, Skakaonica>{



    @Autowired
    private SkakaonicaService skakaonicaService;

  

    @Override
    public Skakaonica convert(SkakaonicaDTO dto) {
    	Skakaonica skakaonica;

        if(dto.getId() == null){
        	skakaonica = new Skakaonica();
        }else{
        	skakaonica = skakaonicaService.findOne(dto.getId());
        }

        if(skakaonica != null){
        	skakaonica.setNaziv(dto.getNaziv());
        	skakaonica.setD(dto.getD());
        	skakaonica.setK(dto.getK());
        	
        	
        }
        return skakaonica;
    }

 
}
