package zima.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Skakaonica;
import zima.web.dto.SkakaonicaDTO;



@Component
public class SKakaonicaToSkakaonicaDTO implements Converter<Skakaonica,SkakaonicaDTO>{



	@Override
	public SkakaonicaDTO convert(Skakaonica skakaonica) {
		
		SkakaonicaDTO skakaonicaDTO= new SkakaonicaDTO();
		skakaonicaDTO.setId(skakaonica.getId());
		skakaonicaDTO.setNaziv(skakaonica.getNaziv());
		skakaonicaDTO.setK(skakaonica.getK());
		skakaonicaDTO.setD(skakaonica.getD());
		
	
		return skakaonicaDTO;
	}
	public List<SkakaonicaDTO> convert(List<Skakaonica> skakaonice){
     List<SkakaonicaDTO> skakaoniceDTO = new ArrayList<>();

     for(Skakaonica skakaonica : skakaonice) {
    	 skakaoniceDTO.add(convert(skakaonica));
     }

     return skakaoniceDTO;
 }
}
