package zima.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Takmicar;
import zima.web.dto.TakmicarDTO;




@Component
public class TakmicarToTakmicarDTO implements Converter<Takmicar,TakmicarDTO> {

	 @Autowired
	    private SKakaonicaToSkakaonicaDTO skakaonicaToSkakaonicaDTO;
	 
	

	@Override
	public TakmicarDTO convert(Takmicar takmicar) {
		
		TakmicarDTO takmicarDTO= new TakmicarDTO();
		takmicarDTO.setId(takmicar.getId());
		takmicarDTO.setDatumRodjenja(takmicar.getDatumRodjenja().toString());
		takmicarDTO.setDrzava(takmicar.getDrzava());
		takmicarDTO.seteMail(takmicar.geteMail());
		takmicarDTO.setImePrezime(takmicar.getImePrezime());
		takmicarDTO.setVisina(takmicar.getVisina());
		takmicarDTO.setSkakaonica(skakaonicaToSkakaonicaDTO.convert(takmicar.getSkakaonica()));
		
		
		
		return takmicarDTO;
	}
	public List<TakmicarDTO> convert(List<Takmicar> takmicari){
     List<TakmicarDTO> takmicariDTO = new ArrayList<>();

     for(Takmicar takmicar : takmicari) {
    	 takmicariDTO.add(convert(takmicar));
     }

     return takmicariDTO;
 }
}
