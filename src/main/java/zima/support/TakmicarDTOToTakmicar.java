package zima.support;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Takmicar;
import zima.service.SkakaonicaService;
import zima.service.TakmicarService;
import zima.web.dto.TakmicarDTO;


@Component
public class TakmicarDTOToTakmicar implements Converter<TakmicarDTO, Takmicar> {


    @Autowired
    private TakmicarService takmicarService;

    @Autowired
    private SkakaonicaService skakaonicaService;
    
   

    @Override
    public Takmicar convert(TakmicarDTO dto) {
    	Takmicar takmicar;

        if(dto.getId() == null){
        	takmicar = new Takmicar();
        }else{
        	takmicar = takmicarService.findOne(dto.getId());
        }

        if(takmicar != null){
        	
			try {
				takmicar.setDatumRodjenja(getDate(dto.getDatumRodjenja()));
	        	takmicar.setDrzava(dto.getDrzava());
	        	takmicar.seteMail(dto.geteMail());
	        	takmicar.setImePrezime(dto.getImePrezime());
	        	takmicar.setVisina(dto.getVisina());
	        	takmicar.setSkakaonica(skakaonicaService.findOne(dto.getSkakaonica().getId()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Greska u konvertoru za TakmicarDTO");
			}

        	
        	
        }
        return takmicar;
    }
   

    private Date getDate(String datumRodjenja) throws ParseException {
    	
    	Date datum = new SimpleDateFormat("yyyy-MM-dd").parse(datumRodjenja);
    	   
    	return datum;
    }
}