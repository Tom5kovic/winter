package zima.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Skok;
import zima.web.dto.SkokDTO;


@Component
public class SkokToSkokDTO implements Converter<Skok,SkokDTO> {

	 @Autowired
	    private TakmicarToTakmicarDTO takmicarToTakmicarDTO;
	 

	@Override
	public SkokDTO convert(Skok skok) {
		
		SkokDTO skokDTO= new SkokDTO();
		skokDTO.setId(skok.getId());
		skokDTO.setDaljina(skok.getDaljina());
		skokDTO.setOcenaSudija(skok.getOcenaSudija());
		skokDTO.setPoeniZaDaljinu(skok.getPoeniZaDaljinu());
		skokDTO.setZbirPoena(skok.getZbirPoena());
		
		skokDTO.setTakmicar(takmicarToTakmicarDTO.convert(skok.getTakmicar()));
		
		
		return skokDTO;
	}
	public List<SkokDTO> convert(List<Skok> skokovi){
     List<SkokDTO> skokoviDTO = new ArrayList<>();

     for(Skok skok : skokovi) {
    	 skokoviDTO.add(convert(skok));
     }

     return skokoviDTO;
 }
}
