package zima.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zima.model.Skok;
import zima.service.SkokService;
import zima.service.TakmicarService;
import zima.web.dto.SkokDTO;



@Component
public class SkokDTOToSKok implements Converter<SkokDTO, Skok>{
	   @Autowired
	    private SkokService skokService;

	    @Autowired
	    private TakmicarService takmicarService;
	    

	    @Override
	    public Skok convert(SkokDTO dto) {
	    	Skok skok;

	        if(dto.getId() == null){
	        	skok = new Skok();
	        }else{
	        	skok = skokService.findOne(dto.getId());
	        }

	        if(skok != null){
	        	skok.setDaljina(dto.getDaljina());
	        	skok.setOcenaSudija(dto.getOcenaSudija());
	        	skok.setPoeniZaDaljinu(dto.getPoeniZaDaljinu());
	        	skok.setZbirPoena(dto.getZbirPoena());
	        	skok.setTakmicar(takmicarService.findOne(dto.getTakmicar().getId()));
	        	        	
	        }
	        return skok;
	    }

	  
}
