package zima.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import zima.model.Skok;
import zima.model.Takmicar;
import zima.service.SkakaonicaService;
import zima.service.SkokService;
import zima.service.TakmicarService;
import zima.support.SkokToSkokDTO;
import zima.support.TakmicarDTOToTakmicar;
import zima.support.TakmicarToTakmicarDTO;
import zima.web.dto.SkokDTO;
import zima.web.dto.TakmicarDTO;


@RestController
@RequestMapping(value = "/api/takmicari", produces = MediaType.APPLICATION_JSON_VALUE)
public class TakmicarController {

	@Autowired
	private SkokToSkokDTO skokToSkokDTO;
	
	@Autowired
	private TakmicarService takmicarService;
	
	@Autowired
	private SkakaonicaService skakaonicaService;
	
	@Autowired
	private SkokService skokService;
	
	@Autowired
	private TakmicarToTakmicarDTO takmicarToTakmicarDTO;
	
	@Autowired
	private TakmicarDTOToTakmicar takmicarDTOToTakmicar;
	
	 @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<TakmicarDTO> create(@Valid @RequestBody TakmicarDTO takmicarDTO){
	        Takmicar takmicar = takmicarDTOToTakmicar.convert(takmicarDTO);

	        if(takmicar.getSkakaonica() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	      
	        Takmicar sacuvanTakmicar = takmicarService.save(takmicar);

	        return new ResponseEntity<>(takmicarToTakmicarDTO.convert(sacuvanTakmicar), HttpStatus.CREATED);
	    }
	 
	

	 @PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<TakmicarDTO> update(@PathVariable Long id, @Valid @RequestBody TakmicarDTO takmicarDTO){

	        if(!id.equals(takmicarDTO.getId())) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }

	        Takmicar takmicar = takmicarDTOToTakmicar.convert(takmicarDTO);

	        if(takmicar.getSkakaonica() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        
	        Takmicar sacuvanTakmicar = takmicarService.update(takmicar);
	        
	     
	        
	       return new ResponseEntity<>(takmicarToTakmicarDTO.convert(sacuvanTakmicar),HttpStatus.OK);
	    }
	 
	 
	 @DeleteMapping("/{id}")
	    public ResponseEntity<Void> delete(@PathVariable Long id){
	        Takmicar obrisanTakmicar = takmicarService.delete(id);

	        if(obrisanTakmicar != null) {
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        } else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	 
	 
	 
	 @GetMapping("/{id}")
	    public ResponseEntity<TakmicarDTO> getOne(@PathVariable Long id){
	        Takmicar takmicar = takmicarService.findOne(id);

	        if(takmicar != null) {
	            return new ResponseEntity<>(takmicarToTakmicarDTO.convert(takmicar), HttpStatus.OK);
	        }else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	 
	 @GetMapping("/{id}/skokovi")
	 public ResponseEntity<List<SkokDTO>> getSkokovi(@PathVariable Long id) {
		 List<Skok> skokovi = skokService.findSkokoviByTakmicarId(id);
		 if (skokovi.size() == 0) {
			 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		 }
		 return new ResponseEntity<>(skokToSkokDTO.convert(skokovi), HttpStatus.OK);
		 
	 }
	 @GetMapping
	    public ResponseEntity<List<TakmicarDTO>> getAll(
	    		@RequestParam(required=false) String datumRodjenjaOd,
	            @RequestParam(required=false) String datumRodjenjaDo,
	            @RequestParam(required=false) Long skakaonicaId,
	            @RequestParam(required=false) String imePrezime,
	            @RequestParam(defaultValue = "0") int pageNum){

	        
	       
	        
	        Page<Takmicar> takmicariPage = takmicarService.pretraga(skakaonicaId,datumRodjenjaOd,datumRodjenjaDo, imePrezime, pageNum);
	        
	        HttpHeaders responseHeaders = new HttpHeaders();
	        responseHeaders.set("Total-Pages", takmicariPage.getTotalPages() + "");
	        
	        return new ResponseEntity<>(takmicarToTakmicarDTO.convert(takmicariPage.getContent()), responseHeaders, HttpStatus.OK);
	    }

}