package zima.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zima.model.Skok;
import zima.service.SkokService;
import zima.support.SkokDTOToSKok;
import zima.support.SkokToSkokDTO;
import zima.web.dto.SkokDTO;

@RestController
@RequestMapping(value = "/api/skokovi", produces = MediaType.APPLICATION_JSON_VALUE)
public class SkokCOntroller {

	@Autowired
	private SkokService skokService;
	
		
	@Autowired
	private SkokToSkokDTO skokToSkokDTO;
	
	@Autowired
	private SkokDTOToSKok skokDTOToSKok;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SkokDTO> create (@Valid @RequestBody SkokDTO skokDTO) {
//		Skok skok =skokDTOToSKok.convert(skokDTO);
		
		Skok sacuvanSkok = skokService.save(skokDTO);
		
		return new ResponseEntity<>(skokToSkokDTO.convert(sacuvanSkok), HttpStatus.OK);
	}

	@GetMapping
    public ResponseEntity<List<SkokDTO>> getAll(){

        List<Skok> skok = skokService.findAll();

        return new ResponseEntity<>(skokToSkokDTO.convert(skok), HttpStatus.OK);
    }
}
