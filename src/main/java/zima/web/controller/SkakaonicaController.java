package zima.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zima.model.Skakaonica;
import zima.service.SkakaonicaService;
import zima.support.SKakaonicaToSkakaonicaDTO;
import zima.support.SkakaonicaDTOToSkakaonica;
import zima.web.dto.SkakaonicaDTO;

@RestController
@RequestMapping(value = "/api/skakaonice", produces = MediaType.APPLICATION_JSON_VALUE)
public class SkakaonicaController {

	@Autowired
	private SkakaonicaService skakaonicaService;
	
		
	@Autowired
	private SKakaonicaToSkakaonicaDTO skakaonicaToSkakaonicaDTO;
	
	@Autowired
	private SkakaonicaDTOToSkakaonica skakaonicaDTOToSkakaonica;
	
	

	@GetMapping
    public ResponseEntity<List<SkakaonicaDTO>> getAll(){

        List<Skakaonica> skakaonica = skakaonicaService.findAll();

        return new ResponseEntity<>(skakaonicaToSkakaonicaDTO.convert(skakaonica), HttpStatus.OK);
    }
	 
}
