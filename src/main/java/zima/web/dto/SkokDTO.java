package zima.web.dto;

import java.util.List;

public class SkokDTO {

	private Long id;
	
	
	private double daljina;
	
	
	private double poeniZaDaljinu;
	
	
	private double ocenaSudija;
	
	private List<Double> pojedinacneOceneSudija;
	
	
	
	
	public List<Double> getPojedinacneOceneSudija() {
		return pojedinacneOceneSudija;
	}


	public void setPojedinacneOceneSudija(List<Double> pojedinacneOceneSudija) {
		this.pojedinacneOceneSudija = pojedinacneOceneSudija;
	}


	private double zbirPoena;
	
	
	private TakmicarDTO takmicar;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public double getDaljina() {
		return daljina;
	}


	public void setDaljina(double daljina) {
		this.daljina = daljina;
	}


	public double getPoeniZaDaljinu() {
		return poeniZaDaljinu;
	}


	public void setPoeniZaDaljinu(double poeniZaDaljinu) {
		this.poeniZaDaljinu = poeniZaDaljinu;
	}


	public double getOcenaSudija() {
		return ocenaSudija;
	}


	public void setOcenaSudija(double ocenaSudija) {
		this.ocenaSudija = ocenaSudija;
	}


	public double getZbirPoena() {
		return zbirPoena;
	}


	public void setZbirPoena(double zbirPoena) {
		this.zbirPoena = zbirPoena;
	}


	public TakmicarDTO getTakmicar() {
		return takmicar;
	}


	public void setTakmicar(TakmicarDTO takmicar) {
		this.takmicar = takmicar;
	}


	public SkokDTO() {
		super();
	}
	
	
}
