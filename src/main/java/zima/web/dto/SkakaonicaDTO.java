package zima.web.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class SkakaonicaDTO {

	private Long id;
	
	@NotNull(message = "Nije zadat naziv skakaonice.")
	private String naziv;
	
	private double k;
	
	private double d;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getK() {
		return k;
	}

	public void setK(double k) {
		this.k = k;
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}

	public SkakaonicaDTO() {
		super();
	}
	
	
	
}
