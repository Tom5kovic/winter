package zima.web.dto;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;



public class TakmicarDTO {

	
	private Long id;
	
	
	private String imePrezime;
	
	
	private String drzava;
	
	
	private double visina;
	
	
	private String eMail;
	
	
	private String datumRodjenja;
	
	
	private SkakaonicaDTO skakaonica;

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getImePrezime() {
		return imePrezime;
	}


	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}


	public String getDrzava() {
		return drzava;
	}


	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}


	public double getVisina() {
		return visina;
	}


	public void setVisina(double visina) {
		this.visina = visina;
	}


	public String geteMail() {
		return eMail;
	}


	public void seteMail(String eMail) {
		this.eMail = eMail;
	}


	public String getDatumRodjenja() {
		return datumRodjenja;
	}


	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}


	public SkakaonicaDTO getSkakaonica() {
		return skakaonica;
	}


	public void setSkakaonica(SkakaonicaDTO skakaonica) {
		this.skakaonica = skakaonica;
	}


	public TakmicarDTO() {
		super();
	}
	
	
	
}
