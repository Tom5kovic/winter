package zima.repository;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import zima.model.Takmicar;


@Repository
public interface TakmicarRepository extends JpaRepository<Takmicar,Long>{

	Takmicar findOneById(Long id);
	
	@Query("SELECT t FROM Takmicar t WHERE "
    		+ "(:skakaonicaId IS NULL OR t.skakaonica.id = :skakaonicaId) AND "
    		+ "(:datumRodjenjaOd IS NULL OR t.datumRodjenja >= :datumRodjenjaOd) AND "
    		+ "(:datumRodjenjaDo IS NULL OR t.datumRodjenja <= :datumRodjenjaDo) AND "
    		+ "(:imePrezime IS NULL OR t.imePrezime like %:imePrezime% )")
    Page<Takmicar> pretraga(@Param("skakaonicaId") Long skakaonicaId, @Param("datumRodjenjaOd") Date datumRodjenjaOd, @Param("datumRodjenjaDo") Date datumRodjenjaDo, @Param("imePrezime") String imePrezime, Pageable pageable);

}
