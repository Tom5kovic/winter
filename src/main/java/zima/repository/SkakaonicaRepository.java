package zima.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zima.model.Skakaonica;

@Repository
public interface SkakaonicaRepository extends JpaRepository<Skakaonica, Long> {

	Skakaonica findOneById(Long id);
}
