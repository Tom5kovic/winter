package zima.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zima.model.Skok;

@Repository
public interface SkokRepository extends JpaRepository<Skok,Long>{
	
	Skok findOneById(Long id);

	List<Skok> findByTakmicarId(Long id);
}
