package zima.service;

import java.util.List;

import zima.model.Skakaonica;



public interface SkakaonicaService {

	List<Skakaonica> findAll();
	
	Skakaonica findOne(Long id);
	
	Skakaonica update(Skakaonica skakaonica);
}
