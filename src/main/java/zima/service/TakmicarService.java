package zima.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import zima.model.Takmicar;



public interface TakmicarService {

	List<Takmicar> findAll();
	 
	Takmicar findOne(Long id);
	 
	Takmicar save(Takmicar takmicar);
	 
	Takmicar update(Takmicar takmicar);
	 
	Takmicar delete(Long id);
	 
	 Page<Takmicar> pretraga(Long skakaonicaId, String datumRodjenjaOd, String datumRodjenjaDo, String imePrezime, int pageNum);
}
