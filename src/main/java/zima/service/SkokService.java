package zima.service;

import java.util.List;

import zima.model.Skok;
import zima.web.dto.SkokDTO;



public interface SkokService {

	List<Skok> findAll();
	
	Skok findOne(Long id);
	
	Skok update(Skok skok);
	
	List<Skok> findSkokoviByTakmicarId(Long id);
	
	Skok save(SkokDTO skokDTO);
}
