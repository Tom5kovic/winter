package zima.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zima.model.Skakaonica;
import zima.repository.SkakaonicaRepository;
import zima.service.SkakaonicaService;

@Service
public class JpaSkakaonicaService implements SkakaonicaService{

	@Autowired 
	private SkakaonicaRepository skakaonicaRepository;
	
	@Override
	public List<Skakaonica> findAll() {
		
		return skakaonicaRepository.findAll();
	}

	@Override
	public Skakaonica findOne(Long id) {
		return skakaonicaRepository.findOneById(id);
	}

	@Override
	public Skakaonica update(Skakaonica skakaonica) {
		return skakaonicaRepository.save(skakaonica);
	}

}
