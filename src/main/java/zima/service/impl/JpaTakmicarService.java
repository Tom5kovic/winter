package zima.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import zima.model.Takmicar;
import zima.repository.TakmicarRepository;
import zima.service.TakmicarService;

@Service
public class JpaTakmicarService implements TakmicarService {

	
	@Autowired
	TakmicarRepository takmicarRepository;
	
	@Override
	public List<Takmicar> findAll() {
		return takmicarRepository.findAll();
	}

	@Override
	public Takmicar findOne(Long id) {
		return takmicarRepository.findOneById(id);
	}

	@Override
	public Takmicar save(Takmicar takmicar) {
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar update(Takmicar takmicar) {
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar delete(Long id) {
		 Optional<Takmicar> takmicar = takmicarRepository.findById(id);
	        if(takmicar.isPresent()){
	        	takmicarRepository.deleteById(id);
	            return takmicar.get();
	        }
	        return null;
	}
	
	
//	 private LocalDate getDateConverted(String date) throws DateTimeParseException {
//	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//	        return LocalDate.parse(date, formatter);
//	    }
	 
	 
	@Override
	public Page<Takmicar> pretraga(Long skakaonicaId, String datumRodjenjaOd, String datumRodjenjaDo,  String imePrezime,
			int pageNum) {

		Date datumOd = null;
		Date datumDo = null;
		
		System.out.println("datumRodjenjaOd: " + datumRodjenjaOd);
		System.out.println("datumRodjenjaDo: " + datumRodjenjaDo);
		try {
			if (datumRodjenjaDo != null && datumRodjenjaOd != null) {
				datumOd = getDate(datumRodjenjaOd);
				datumDo = getDate(datumRodjenjaDo);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Datum od: " + datumOd);
		System.out.println("Datum do: " + datumDo);
		return takmicarRepository.pretraga(skakaonicaId, datumOd, datumDo, imePrezime, PageRequest.of(pageNum, 5));
	}
	
    private Date getDate(String datumRodjenja) throws ParseException {
    	
    	Date datum = new SimpleDateFormat("yyyy-MM-dd").parse(datumRodjenja);
    	   
    	return datum;
    }

}
